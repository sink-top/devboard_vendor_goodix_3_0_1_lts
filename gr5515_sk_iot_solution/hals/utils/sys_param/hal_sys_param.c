/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Description: implement of hal_sys_param
 */

#include <stdio.h>
#include "securec.h"
#include "hal_sys_param.h"
#include "device_profile.h"

static const char OHOS_HARDWARE_PROFILE[] = {"aout:true,display:true"};
static const char OHOS_BOOTLOADER_VERSION[] = {"bootloader"};
static const char OHOS_ABI_LIST[] = {"arm"};
static const int OHOS_FIRST_API_VERSION = 1;
static const char EMPTY_STR[] = {""};

const char *HalGetDeviceType(void)
{
    return DEVICE_TYPE_ID;
}

const char *HalGetManufacture(void)
{
    return MANUAFACTURER_NAME;
}

const char *HalGetBrand(void)
{
    return PRODUCT_BRAND;
}

const char *HalGetMarketName(void)
{
    return DEVICE_NAME;
}

const char *HalGetProductSeries(void)
{
    return PRODUCT_SERIES;
}

const char *HalGetProductModel(void)
{
    return PRODUCT_MODEL;
}

const char *HalGetSoftwareModel(void)
{
    return SOFTWARE_VER;
}

const char *HalGetHardwareModel(void)
{
    return HARDWARE_VER;
}

const char *HalGetHardwareProfile(void)
{
    return OHOS_HARDWARE_PROFILE;
}

const char *HalGetSerial(void)
{
    return PRODUCT_SN;
}

const char *HalGetBootloaderVersion(void)
{
    return OHOS_BOOTLOADER_VERSION;
}

const char *HalGetAbiList(void)
{
    return OHOS_ABI_LIST;
}

const char *HalGetDisplayVersion(void)
{
    return DEVICE_DISPLAY_VERSION;
}

const char *HalGetIncrementalVersion(void)
{
    return INCREMENTAL_VERSION;
}

const char *HalGetBuildType(void)
{
    return BUILD_TYPE;
}

const char *HalGetBuildUser(void)
{
    return BUILD_USER;
}

const char *HalGetBuildHost(void)
{
    return BUILD_HOST;
}

const char *HalGetBuildTime(void)
{
    return BUILD_TIME;
}

int HalGetFirstApiVersion(void)
{
    return OHOS_FIRST_API_VERSION;
}
