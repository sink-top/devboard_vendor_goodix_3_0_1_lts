/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Description: implement of oem_auth_config
 * Author: Kit Framework group
 * Create: 2020-12-26
 */

#include "oem_auth_config.h"

#include <stdio.h>
#include <stdlib.h>
#include "securec.h"

#include "cJSON.h"

#define TOTAL_TIMEOUT 10000

static const char *KitInfos[][KIT_INFO_PAIR_LEN] = {
    {"HiLinkKit", "1.0.0"},
};

int32_t OEMReadAuthServerInfo(char *buff, uint32_t len)
{
    printf("Enter Function OEMReadAuthServerInfo\n");
    if (buff == NULL || len == 0) {
        return -1;
    }
    return -1;
}

char *OEMLoadKitInfos(void)
{
    printf("Enter Function OEMLoadKitInfos\n");
    cJSON *root = cJSON_CreateObject();
    if (root == NULL) {
        return NULL;
    }
    cJSON *infos = cJSON_CreateArray();
    if (infos == NULL) {
        cJSON_Delete(root);
        return NULL;
    }

    const uint32_t kitCount = sizeof(KitInfos) / (sizeof(char *) * KIT_INFO_PAIR_LEN);
    for (uint32_t i = 0; i < kitCount; i++) {
        cJSON *obj = cJSON_CreateObject();
        if (obj == NULL) {
            printf("OEM Adapter: Create json object failed\n");
            continue;
        }
        const char *key = KitInfos[i][KIT_INFO_PAIR_KEY];
        const char *value = KitInfos[i][KIT_INFO_PAIR_VAL];
        if (key == NULL || value == NULL) {
            printf("OEM Adapter: Add kit info %s to json object failed\n", key);
            cJSON_Delete(obj);
            continue;
        }
        cJSON_AddStringToObject(obj, key, value);
        cJSON_AddItemToArray(infos, obj);
    }

    cJSON_AddItemToObject(root, KIT_INFO_JSON_KEY, infos);
    char *result = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);
    return result;
}