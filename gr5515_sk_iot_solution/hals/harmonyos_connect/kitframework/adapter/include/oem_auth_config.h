/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Description: headfile of oem_auth_config
 */

#ifndef HOS_LITE_OEM_AUTH_CONFIG_H
#define HOS_LITE_OEM_AUTH_CONFIG_H

#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define KIT_INFO_JSON_KEY "KitInfos"
#define KIT_INFO_PAIR_KEY 0
#define KIT_INFO_PAIR_VAL 1
#define KIT_INFO_PAIR_LEN 2

/**
 * @brief read authentication's server info from device which implemented by OEM.
 *        format: server1:port1;server2:port2; ...
 * @param port: pointer of server info's buffer.
 * @param len: length of server info's buffer, max value is 256.
 *             The max length for signal NV srtuct is 252 Bytes, so please be careful if the server info is save as NV.
 * @return 0 is read succeed, others is failed.
 */
int32_t OEMReadAuthServerInfo(char *buff, uint32_t len);

/*
 * @brief load kit's id and verison information from device, when the device has the kit, definition follow #define.
 * #define HCS_DVKIT
 * #define HCS_HILINKKIT
 * @return char* not NULL means load succeed, NULL is means OEM haven't set kit infos or no others kit to authenticate
 *         example : NULL means no others kit
 *                   "{\"KitInfos\":[]}" means no others kit
 *                   "{\"KitInfos\":[{\"HiLinkKit\":\"1.0.0\"},{\"DvKit\":\"1.0.1\"}]}" means with HiLink and DvKit
 */
char *OEMLoadKitInfos(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* HOS_LITE_OEM_AUTH_CONFIG_H */
