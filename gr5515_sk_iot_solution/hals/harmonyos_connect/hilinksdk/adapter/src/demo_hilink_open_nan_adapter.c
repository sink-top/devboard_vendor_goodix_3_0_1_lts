/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Description: NAN 适配接口函数(需设备厂商实现)
 */

#include "hilink_open_nan_adapter.h"
#include <stdlib.h>

int HILINK_NanInit(void)
{
    return 0;
}

int HILINK_NanUninit(void)
{
    return 0;
}

int HILINK_NanStartSubscribe(const char *serviceName, unsigned char localHandle, void *handle)
{
    if ((serviceName == NULL) || (handle == NULL)) {
        return -1;
    }

    return 0;
}

int HILINK_NanStopSubscribe(unsigned char localHandle)
{
    return 0;
}

int HILINK_NanSendPacket(
    unsigned char *macAddr, unsigned char peerHandle, unsigned char localHandle, unsigned char *msg, int len)
{
    if ((macAddr == NULL) || (msg == NULL)) {
        return -1;
    }

    return 0;
}

int HILINK_NanSwitchSafeDistance(void)
{
    return 0;
}

int HILINK_NanSwitchNormalDistance(void)
{
    return 0;
}

int HILINK_NanBeaconSwitch(unsigned char enable)
{
    return 0;
}

int HILINK_SetSafeDistancePower(signed char power)
{
    return 0;
}

int HILINK_SoftApDeauthStation(const unsigned char *mac, unsigned char len)
{
    if (mac == NULL) {
        return -1;
    }

    return 0;
}

void HILINK_SetNanIdentifyStatus(int enable)
{
    (void)enable;
}

unsigned int HILINK_GetNanAbility(void)
{
    return 1;
}

int HILINK_SetBeaconPeriod(void)
{
    return 0;
}