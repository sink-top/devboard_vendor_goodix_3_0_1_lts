/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DEVICE_PROFILE_H
#define DEVICE_PROFILE_H

/* AC参数 */
static const unsigned char PRODUCT_ACKEY[48] = {};
/* PRODKEY参数 */
static const char *PRODUCT_PRODKEY = "";
/* 设备产品ID */
static const char *PRODUCT_ID = "";
/* 设备产品子型号ID */
static const char *SUB_PRODUCT_ID = "";
/* 设备类型ID */
static const char *DEVICE_TYPE_ID = "";
/* 设备类型英文名称 */
static const char *DEVICE_TYPE_NAME = "";
/* 设备制造商ID */
static const char *MANUAFACTURER_ID = "";
/* 设备制造商英文名称 */
static const char *MANUAFACTURER_NAME = "";
/* 设备型号 */
static const char *PRODUCT_MODEL = "";
/* 设备SN */
static const char *PRODUCT_SN = "";
/* 设备固件版本号 */
static const char *FIRMWARE_VER = "";
/* 设备硬件版本号 */
static const char *HARDWARE_VER = "";
/* 设备软件版本号 */
static const char *SOFTWARE_VER = "";
/* 设备产品名称 */
static const char *DEVICE_NAME = "";
/* 设备产品品牌英文名 */
static const char *PRODUCT_BRAND = "";
/* 设备产品所属产品系列 */
static const char *PRODUCT_SERIES = "";
/* 设备所用操作系统名称 */
static const char *DEVICE_OS = "";
/* 设备所用操作系统版本号 */
static const char *DEVICE_OS_VERSION = "";
/* 设备所用操作系统显示全称 */
static const char *DEVICE_DISPLAY_VERSION = "";

#endif /* DEVICE_PROFILE_H */
