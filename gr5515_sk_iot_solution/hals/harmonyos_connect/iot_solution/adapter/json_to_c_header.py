#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import re
import json
from sys import argv

print("Start to change json file to c header.")
project_dir = argv[1] + "/harmonyos_connect/iot_solution/adapter/"
# device_profile.h路径
device_profile_path = project_dir + "include/device_profile.h"
# 存放json文件的目录
json_file_path = project_dir + "json/Product_" + argv[2] + ".json"
print(json_file_path)

file_data = ""
with open(device_profile_path, encoding="utf-8") as f1, open(json_file_path, encoding="utf-8") as f2:
    content = json.load(f2)
    for line in f1:
        if "PRODUCT_ACKEY[48]" in line:
            PRODUCT_ACKEY = content["acKey"]
            temp = "0x"
            res = "{"
            for i in range(len(PRODUCT_ACKEY)):
                temp += PRODUCT_ACKEY[i]
                if i % 2 == 1:
                    res += (temp + ", ")
                    temp = "0x"

            res = res[:-1]
            print(res)
            res = res[:72] + "\n   " + res[72:186] + "\n   " + res[186:-1] + "};\n"
            print(res)
            line = "static const unsigned char PRODUCT_ACKEY[48] = " + res
            print(line)
            print("===============")
        if ", 0x" in line and "PRODUCT_ACKEY[48]" not in line:
            continue
        if "PRODUCT_PRODKEY" in line:
            PRODUCT_PRODKEY = content["prodKey"]
            line = re.sub('".*?"', '"%s"' % PRODUCT_PRODKEY, line)
            print(line)
        if "PRODUCT_ID" in line:
            PRODUCT_ID = content["prodId"]
            line = re.sub('".*?"', '"%s"' % PRODUCT_ID, line)
            print(line)
        if "SUB_PRODUCT_ID" in line:
            SUB_PRODUCT_ID = ""
            line = re.sub('".*?"', '"%s"' % SUB_PRODUCT_ID, line)
            print(line)
        if "DEVICE_TYPE_ID" in line:
            DEVICE_TYPE_ID = content["deviceTypeId"]
            line = re.sub('".*?"', '"%s"' % DEVICE_TYPE_ID, line)
            print(line)
        if "DEVICE_TYPE_NAME" in line:
            DEVICE_TYPE_NAME = content["deviceTypeNameEn"]
            line = re.sub('".*?"', '"%s"' % DEVICE_TYPE_NAME, line)
            print(line)
        if "MANUAFACTURER_ID" in line:
            MANUAFACTURER_ID = content["manufacturerId"]
            line = re.sub('".*?"', '"%s"' % MANUAFACTURER_ID, line)
            print(line)
        if "MANUAFACTURER_NAME" in line:
            MANUAFACTURER_NAME = content["manufacturerNameEn"]
            line = re.sub('".*?"', '"%s"' % MANUAFACTURER_NAME, line)
            print(line)
        if "PRODUCT_MODEL" in line:
            PRODUCT_MODEL = content["deviceModel"]
            line = re.sub('".*?"', '"%s"' % PRODUCT_MODEL, line)
            print(line)
        if "PRODUCT_SN" in line:
            PRODUCT_SN = "1234567890AB"
            line = re.sub('".*?"', '"%s"' % PRODUCT_SN, line)
            print(line)
        if "FIRMWARE_VER" in line:
            FIRMWARE_VER = "1.0.0"
            line = re.sub('".*?"', '"%s"' % FIRMWARE_VER, line)
            print(line)
        if "HARDWARE_VER" in line:
            HARDWARE_VER = "1.0.0"
            line = re.sub('".*?"', '"%s"' % HARDWARE_VER, line)
            print(line)
        if "SOFTWARE_VER" in line:
            SOFTWARE_VER = "1.0.0"
            line = re.sub('".*?"', '"%s"' % SOFTWARE_VER, line)
            print(line)
        if "DEVICE_NAME" in line:
            DEVICE_NAME = content["deviceName"]
            line = re.sub('".*?"', '"%s"' % DEVICE_NAME, line)
            print(line)
        if "PRODUCT_BRAND" in line:
            PRODUCT_BRAND = content["brandEn"]
            line = re.sub('".*?"', '"%s"' % PRODUCT_BRAND, line)
            print(line)
        if "PRODUCT_SERIES" in line:
            PRODUCT_SERIES = content["productSeries"]
            line = re.sub('".*?"', '"%s"' % PRODUCT_SERIES, line)
            print(line)
        if "DEVICE_OS" in line:
            DEVICE_OS = content["os"]
            line = re.sub('".*?"', '"%s"' % DEVICE_OS, line)
            print(line)
        if "DEVICE_OS_VERSION" in line:
            DEVICE_OS_VERSION = content["osVersion"]
            line = re.sub('".*?"', '"%s"' % DEVICE_OS_VERSION, line)
            print(line)
        if "DEVICE_DISPLAY_VERSION" in line:
            DEVICE_DISPLAY_VERSION = content["os"] + " " + content["osVersion"]
            line = re.sub('".*?"', '"%s"' % DEVICE_DISPLAY_VERSION, line)
            print(line)
        file_data += line

with open(device_profile_path, mode="w", encoding="utf-8") as f:
    f.write(file_data)
print("Change json file to c header end.")
