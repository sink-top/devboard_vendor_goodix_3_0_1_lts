# devboard_vendor_goodix_gr5515_sk_basic

## 开发板介绍

### 开发板概述
GR5515 Starter Kit（以下简称GR5515 SK）套件是基于的GR551x芯片（支持Bluetooth 5.1）设计的开发平台，包含Starter Kit开发板（以下简称GR5515 SK板）、原理图和使用指南。用户可以在该平台上熟悉GR551x开发工具以及快速搭建自己的产品原型并验证相关功能。

### 开发板功能

*  支持Bluetooth 5.1的单模低功耗蓝牙SoC
*  多功能按键和LED指示灯
*  支持Arduino模块插接接口，IO电压可以通过level shift灵活配置
*  支持调试功能的SEGGER J-Link OB
*  UART转USB接口
*  Micro USB接口连接PC
*  1.44寸TFT彩色显示屏
*  板上集成QSPI Flash

为了更好的使用GR5515 Starter Kit套件，建议参考下表相关资料。

|            名称            |                                                                                       描述                                                                                        |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| GR5515 Starter Kit用户指南 | 介绍GR5515 Starter Kit套件使用方法： [《GR5515 Starter Kit用户指南》]( https://docs.goodix.com/zh/online/detail/gr5515_starter_kit_user_guide/V1.7/42a03ba92cad1d63afd9baa8bb8c37df) |
| GR551x开发者指南           | GR551x软硬件介绍、快速使用及资源总览： [《GR551x开发者指南》]( https://docs.goodix.com/zh/online/detail/gr551x_develop_guide/V2.3/27f7d503bcd7ad1d63fa5b316b3bde4f)                    |
| J-Link用户指南             | J-Link使用说明：www.segger.com/downloads/jlink/UM08001_JLink.pdf                                                                                                                   |
| GR5515-SK-BASIC-RevC      | GR5515 Starter Kit开发板原理图：[《GR5515-SK-BASIC-RevC.pdf》]( https://product.goodix.com/zh/docview/GR5515-SK-BASIC-RevC_Rev.1.5?objectId=100&objectType=document&version=133)   |


## 相关仓库

[devboard_device_goodix_gr551x](https://gitee.com/openharmony-sig/devboard_device_goodix_gr551x)

[devboard_vendor_goodix_gr5515_sk_basic](https://gitee.com/openharmony-sig/devboard_vendor_goodix_gr5515_sk_basic)

